#include "..\Interfaces\Translate.h"

namespace Geometry
{
	void Translate::DoTransform(const std::shared_ptr<Model>& model)
	{
		model->LoopVertices([&](shared_ptr<Vector3>& vector) {
			vector->x += moveVec.x;
			vector->y += moveVec.y;
			vector->z += moveVec.z;
			});
	}
}