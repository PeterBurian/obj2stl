#include "..\Interfaces\Rotate.h"
#include <math.h>

namespace Geometry
{
	void Rotate::DoTransform(const std::shared_ptr<Model>& model)
	{
		model->LoopVertices([&](shared_ptr<Vector3>& vec) {
			// Z
			if (rotRad.z != 0)
			{
				vec->x = vec->x * cos(rotRad.z) - vec->y * sin(rotRad.z);
				vec->y = vec->x * sin(rotRad.z) + vec->y * cos(rotRad.z);
			}

			// X
			if (rotRad.x != 0)
			{
				vec->y = vec->y * cos(rotRad.x) - vec->z * sin(rotRad.x);
				vec->z = vec->y * sin(rotRad.x) + vec->z * cos(rotRad.x);
			}

			// Y
			if (rotRad.y != 0)
			{
				vec->x = vec->x * cos(rotRad.y) + vec->z * sin(rotRad.y);
				vec->z = -vec->x * sin(rotRad.y) + vec->z * cos(rotRad.y);
			}
			});
	}
}