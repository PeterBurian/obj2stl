#include "..\Interfaces\Scale.h"

namespace Geometry
{
	void Scale::DoTransform(const std::shared_ptr<Model>& model)
	{
		model->LoopVertices([&](shared_ptr<Vector3>& vec) {
			vec->x *= scaleFactor.x;
			vec->y *= scaleFactor.y;
			vec->z *= scaleFactor.z;
			});
	}
}