#include "../Interfaces/Calculator.h"
#include "../Models/Plane.h"
#include "../Models/Tetrahedron.h"
#include <iostream>

namespace Geometry
{
	double Calculator::Surface(const std::shared_ptr<Model>& model)
	{
		double surface = .0;
		
		if (model != nullptr)
		{
			model->LoopTriangles([&](const shared_ptr<Triangle>& tri) 
				{
					surface += tri->Area();
				});
		}
		else
		{
			std::cout << "The model is not avaible" << std::endl;
		}
		
		return surface;
	}

	double Calculator::Volume(const std::shared_ptr<Model>& model)
	{
        double volume = .0;

		if (model != nullptr)
		{
            Vector3 center = model->GetCenter();

			model->LoopTriangles([&](const shared_ptr<Triangle>& tri)
				{
					Tetrahedron tetrahedron(tri->A(), tri->B(), tri->C(), center);
					volume += tetrahedron.GetVolume();
				});
		}
		else
		{
			std::cout << "The model is not avaible" << std::endl;
		}

        return volume;
	}

	bool Calculator::IsPointInModel(const Vector3& point, const std::shared_ptr<Model>& model)
	{
        if (model != nullptr)
        {
            double minDist = -1;
            shared_ptr<Triangle> selectedTriangle = nullptr;

            Vector3 center = model->GetCenter();
			model->LoopTriangles([&](const shared_ptr<Triangle>& tri) 
				{
					double distance = Vector3::Distance(tri->Center(), point);
					if (distance < minDist || minDist == -1)
					{
						minDist = distance;
						selectedTriangle = tri;
					}
				});
			
            //Based on thesis that the center of the model is inside the mesh
            if (selectedTriangle != nullptr)
            {
                auto plane = std::make_unique<Plane>(selectedTriangle->A(), selectedTriangle->B(), selectedTriangle->C());
                return plane->SameSide(point, center);
            }
        }
        else
        {
            std::cout << "The model is not avaible" << std::endl;
            throw std::exception("The model is not avaible");
        }

		return false;
	}
}