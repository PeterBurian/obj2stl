#pragma once

#include "ITransfromation.h"

namespace Geometry
{
	class Rotate : public ITransfromation
	{
	public:
		Rotate() { rotRad = Vector3::One; }
		Rotate(const Vector3& rot) { rotRad = rot; }
		~Rotate() { }

		/// <summary>
		/// 
		/// Axis X
		/// (1,    0,	 0, 0)
		/// (0,  cos, -sin, 0)
		/// (0,  sin,  cos, 0)
		/// (0,    0,    0, 1)
		/// 
		/// Axis Y
		/// (cos,   0,  sin, 0)
		/// (0,     1,    0, 0)
		/// (-sin,  0,  cos, 0)
		/// (0,     0,    0, 1)
		/// 
		/// Axis Z
		/// (cos, -sin,	 0, 0)
		/// (sin,  cos,  0, 0)
		/// (0,    0,    1, 0)
		/// (0,    0,    0, 1)
		/// </summary>
		/// <param name="model"></param>
		void DoTransform(const std::shared_ptr<Model>& model) override;

	private:
		Vector3 rotRad;
	};
}