#pragma once

#include "Model.h"
#include<memory>

namespace Geometry
{
	class Calculator
	{
	public:
		Calculator() { }

		/// <summary>
		/// Calculate surface of the model
		/// </summary>
		/// <returns>Returns the surface of the model</returns>
		double Surface(const std::shared_ptr<Model>& model);

		/// <summary>
		/// Calculate volume of the model
		/// </summary>
		/// <returns>Returns the volume of the model</returns>
		double Volume(const std::shared_ptr<Model>& model);

		/// <summary>
		/// Checks vector is in model
		/// </summary>
		/// <param name="point"></param>
		/// <param name="model"></param>
		/// <returns>Returns true if the given point is inside the model. Otherwise false.</returns>
		bool IsPointInModel(const Vector3& point, const std::shared_ptr<Model>& model);
	};
}