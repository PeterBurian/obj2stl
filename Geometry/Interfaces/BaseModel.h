#pragma once

namespace Geometry
{
	class BaseModel
	{
	public:
		BaseModel() { }
		virtual ~BaseModel() { }
	};
}