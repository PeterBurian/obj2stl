#pragma once

#include "ITransfromation.h"
#include "../Models/Vector3.h"

namespace Geometry
{
	class Translate : public ITransfromation
	{
	public:
		Translate() { moveVec = Vector3::Zero; }
		Translate(const Vector3& vec) { moveVec = vec; }
		~Translate() { }

		/// <summary>
		/// (1,	 0,	 0,  0)
		/// (0,  1,  0,  0)
		/// (0,  0,  1,  0)
		/// (Tx, Ty, Tz, 1)
		/// </summary>
		/// <param name="model"></param>
		void DoTransform(const std::shared_ptr<Model>& model) override;

	private:
		Vector3 moveVec;
	};
}