#pragma once

#include "ITransfromation.h"

namespace Geometry
{
	class Scale : public ITransfromation
	{
	public:
		Scale() { scaleFactor = Vector3::One; }
		Scale(const Vector3& factor) { scaleFactor = factor; }
		~Scale() { }

		/// <summary>
		/// (Sx, 0,	 0,  0)
		/// (0,  Sy, 0,  0)
		/// (0,  0,  Sz, 0)
		/// (0,  0,  0,  1)
		/// </summary>
		/// <param name="model"></param>
		void DoTransform(const std::shared_ptr<Model>& model) override;
	private:
		Vector3 scaleFactor;
	};
}