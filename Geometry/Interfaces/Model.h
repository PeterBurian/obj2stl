#pragma once

#include "BaseModel.h"
#include "../Models/Group.h"
#include "../Models/Face.h"
#include "../Models/Vector3.h"
#include "../Models/Triangle.h"

#include <list>
#include <vector>
#include <memory>

using namespace std;
namespace Geometry
{
	class Model : public BaseModel
	{
	public:
		Model();
		~Model() { }

		/// <summary>
		/// Add group to model
		/// </summary>
		/// <param name="group"></param>
		void AddGroup(const shared_ptr<Group>& group);

		/// <summary>
		/// Add vertex to model
		/// </summary>
		/// <param name="vector"></param>
		void AddVertex(const shared_ptr<Vector3>& vector);

		/// <summary>
		/// Add face to model
		/// </summary>
		/// <param name="face"></param>
		void AddFace(const shared_ptr<Face>& face);

		/// <summary>
		/// Add normal vectors to model
		/// </summary>
		/// <param name="vector"></param>
		void AddNormal(const shared_ptr<Vector3>& vector);

		/// <summary>
		/// Add texture vectors to model
		/// </summary>
		/// <param name="vector"></param>
		void AddTextureVertex(const shared_ptr<Vector3>& vector);

		/// <summary>
		/// Get Stored vertex by its index
		/// </summary>
		/// <param name="idx">index of vertex</param>
		/// <param name="vertex">out vertex</param>
		/// <returns>Returns true if the vertex is exists</returns>
		bool GetVertexByIndex(const unsigned int idx, shared_ptr<Vector3>& vertex);

		/// <summary>
		/// Get the count of the groups
		/// </summary>
		/// <returns>Returns the count of the groups</returns>
		size_t GroupsCount();

		/// <summary>
		/// Create triangles from faces
		/// </summary>
		void CreateTriangles();

		/// <summary>
		/// Triangulate polygons
		/// </summary>
		/// <param name="polygon"></param>
		/// <param name="face"></param>
		void Triangulate(const vector<shared_ptr<Vector3>>& polygon, const shared_ptr<Face>& face);

		/// <summary>
		/// Clear verticies buffer
		/// </summary>
		void ClearStoredVertices();

		/// <summary>
		/// Get the number of triangles
		/// </summary>
		/// <returns>Returns the number of triangles</returns>
		size_t GetNumberOfTriangles();

        /// <summary>
        /// Get the center of the model
        /// </summary>
        /// <returns>Returns the center of the model</returns>
        Vector3 GetCenter();

        /// <summary>
        /// Get the exenets of the model
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        void GetExtents(Vector3& min, Vector3& max);

		/// <summary>
		/// Loop the faces of the model
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		template<class T>
		void LoopFaces(T func)
		{
			for (auto itGroup = this->groups.begin(); itGroup != this->groups.end(); ++itGroup)
			{
				list<shared_ptr<Face>> faces = (*itGroup)->faces;
				for (auto itFace = faces.begin(); itFace != faces.end(); ++itFace)
				{
					func(*itFace);
				}
			}
		}

		/// <summary>
		/// Loop the triangles of the model
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		template<class T>
		void LoopTriangles(T func)
		{
			LoopFaces([&](shared_ptr<Face> face)
				{
					for (auto itTri = face->triangles.begin(); itTri != face->triangles.end(); ++itTri)
					{
						func(*itTri);
					}
				});
		}

		/// <summary>
		/// Loop vertices of the model
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="func"></param>
		template<class T>
		void LoopVertices(T func)
		{
			for (auto itVertex = this->verticesBuffer.begin(); itVertex != this->verticesBuffer.end(); ++itVertex)
			{
				func(*itVertex);
			}
		}

	private:
		shared_ptr<Group> ActiveGroup;

		vector<shared_ptr<Vector3>> verticesBuffer;
		list<shared_ptr<Group>> groups;
		vector<shared_ptr<Vector3>> normals;
		vector<shared_ptr<Vector3>> textureVertices;

		/// <summary>
		/// Creates an empty group.
		/// </summary>
		void AddEmptyGroup();
	};
}