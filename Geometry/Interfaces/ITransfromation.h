#pragma once

#include<memory>
#include "Model.h"

namespace Geometry
{
	class ITransfromation
	{
	public:
		virtual ~ITransfromation() { }

		/// <summary>
		/// Do the given transformation on the given model
		/// </summary>
		/// <param name="model">model</param>
		virtual void DoTransform(const std::shared_ptr<Model>& model) = 0;
	};
}