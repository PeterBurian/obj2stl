#pragma once

#include "../Interfaces/BaseModel.h"
#include "Face.h"
#include <list>
#include <string>

using namespace std;
namespace Geometry
{
	class Group : public BaseModel
	{
	public:
		Group() { }
		Group(const string& title);
		~Group() { }

		Group(const Group& other) { *this = other; }

		Group& operator=(const Group& other);

		/// <summary>
		/// Add face
		/// </summary>
		/// <param name="face">face</param>
		void AddFace(const shared_ptr<Face>& face);

		/// <summary>
		/// The active face of the model
		/// </summary>
		shared_ptr<Face> ActiveFace;

		/// <summary>
		/// List of faces in the given group
		/// </summary>
		list<shared_ptr<Face>> faces;

	private:
		/// <summary>
		/// Title of the group
		/// </summary>
		string title;
	};
}
