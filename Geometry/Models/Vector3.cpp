#include "Vector3.h"

#include <math.h>

namespace Geometry
{
	const double Vector3::epsilon = 0.00001;
	const Vector3 Vector3::Zero = Vector3();
	const Vector3 Vector3::One = Vector3(1.0, 1.0, 1.0);

	void Vector3::Normalize()
	{
		double mag = Magnitude(*this);
		if (mag > epsilon)
		{
			*this = *this / mag;
		}
		else
		{
			*this = Zero;
		}
	}

	double Vector3::Magnitude(const Vector3& vec)
	{
		return sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	}

	double const Vector3::Distance(const Vector3& a, const Vector3& b)
	{
		double diff_x = a.x - b.x;
		double diff_y = a.y - b.y;
		double diff_z = a.z - b.z;

		return sqrt(diff_x * diff_x + diff_y * diff_y + diff_z * diff_z);
	}

	bool Vector3::Equals(const Vector3& a, const Vector3 b)
	{
		return Distance(a, b) < epsilon;
	}

    double Vector3::Dot(const Vector3& lhs, const Vector3& rhs)
    {
       return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
    }

	Vector3 Vector3::Cross(const Vector3& lhs, const Vector3& rhs)
	{
		return Vector3(
			lhs.y * rhs.z - lhs.z * rhs.y,
			lhs.z * rhs.x - lhs.x * rhs.z,
			lhs.x * rhs.y - lhs.y * rhs.x);
	}
}