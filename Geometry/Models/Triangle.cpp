#include "Triangle.h"
#include <iostream>

namespace Geometry
{
	Triangle::Triangle(vector<shared_ptr<Vector3>> items)
	{
		corners = items;
	}

	Triangle& Triangle::operator=(const Triangle& other)
	{
		if (this != &other)
		{
			this->corners = other.corners;
		}
		return *this;
	}

	const Vector3 Triangle::A()
	{
		return GetCoordByIdx(0);
	}

	const Vector3 Triangle::B()
	{
		return GetCoordByIdx(1);
	}

	const Vector3 Triangle::C()
	{
		return GetCoordByIdx(2);
	}

	bool Triangle::IsValid()
	{
		return corners.size() == 3;
	}

	double Triangle::Area()
	{
		if (IsValid())
		{
            return Area(*corners[0], *corners[1], *corners[2]);
		}
		return .0;
	}

	Vector3 Triangle::Normal()
	{
		Vector3 cross = Vector3::Cross(B() - A(), C() - A());
		cross.Normalize();
		return cross;
	}

    Vector3 Triangle::Center()
    {
        if (IsValid())
        {
            return (A() + B() + C()) / 3;
        }
        return Vector3::Zero;
    }

    double Triangle::Area(const Vector3& A, const Vector3& B, const Vector3& C)
    {
        double sideA = Vector3::Distance(B, C);
        double sideB = Vector3::Distance(A, C);
        double sideC = Vector3::Distance(A, B);

        return Area(sideA, sideB, sideC);
    }

    inline double Triangle::Area(const double a, const double b, const double c)
    {
        double s = (a + b + c) / 2;
        return sqrt(s * (s - a) * (s - b) * (s - c));
    }

    Vector3 Triangle::GetCoordByIdx(const unsigned int idx)
	{
		if (IsValid())
		{
			return *corners[idx];
		}

		//Should be some error handling here
		return Vector3::Zero;
	}
}