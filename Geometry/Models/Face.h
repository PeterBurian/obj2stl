#pragma once

#include "../Interfaces/BaseModel.h"
#include "Vector3.h"
#include "Triangle.h"

#include <memory>
#include <vector>

using namespace std;
namespace Geometry
{
	class Face : public BaseModel
	{
	public:
		vector<shared_ptr<Triangle>> triangles;
		vector<shared_ptr<Vector3>> points;

		Face();
		~Face();

		/// <summary>
		/// Add vertex to face
		/// </summary>
		/// <param name="vertex"></param>
		void AddVertex(shared_ptr<Vector3>& vertex);

		/// <summary>
		/// Add triangle to the face
		/// </summary>
		/// <param name="triangle">triangle</param>
		void AddTriangle(shared_ptr<Triangle>& triangle);

		/// <summary>
		/// Get the normal of the triangle
		/// </summary>
		/// <returns>Returns the normal of the triangle</returns>
		Vector3 GetNormal();
	};
}