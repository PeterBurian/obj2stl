#pragma once

#include "../Interfaces/BaseModel.h"

namespace Geometry
{
	class Vector3 : public BaseModel
	{	
	public:
		static const double epsilon;
		static const Vector3 Zero;
		static const Vector3 One;

		double x;
		double y;
		double z;

		Vector3() : Vector3(.0, .0, .0) { }
		Vector3(double x, double y, double z) : x(x), y(y), z(z) { }
		~Vector3() { }

		Vector3 operator+(const Vector3& other) const { return { x + other.x, y + other.y, z + other.z }; }
		Vector3 operator-(const Vector3& other) const { return { x - other.x, y - other.y, z - other.z }; }
		Vector3 operator*(double f) const { return { x * f, y * f, z * f }; }
		Vector3 operator/(double f) const { return { x / f, y / f, z / f }; }

		Vector3(const Vector3& other) { *this = other; }

		Vector3& operator=(const Vector3& other)
		{
			if (this != &other)
			{
				x = other.x;
				y = other.y;
				z = other.z;
			}
			return *this;
		}

		/// <summary>
		/// Normalize the vector
		/// </summary>
		void Normalize();

		/// <summary>
		/// Get magnitude
		/// </summary>
		/// <param name="vec">Vector3 vec</param>
		/// <returns>Returns the magnitude of the vector</returns>
		static double Magnitude(const Vector3& vec);
		
		/// <summary>
		/// Get the distance of the given vectors
		/// </summary>
		/// <param name="vec">Vector3 a</param>
		/// <param name="vec">Vector3 b</param>
		/// <returns>Returns the distance of the vectors</returns>
		static const double Distance(const Vector3& a, const Vector3& b);

		/// <summary>
		/// Compare two vector
		/// </summary>
		/// <param name="a">Vector3 a</param>
		/// <param name="b">Vector3 b</param>
		/// <returns>Returns true if the 2 vectors are the same coordinates</returns>
		static bool Equals(const Vector3& a, const Vector3 b);

		/// <summary>
		/// Cross products of 2 vector
		/// </summary>
		/// <param name="lhs">vector 1</param>
		/// <param name="rhs">vector 2</param>
		/// <returns>Returns the cross products of the given vectors</returns>
		static Vector3 Cross(const Vector3& lhs, const Vector3& rhs);

        /// <summary>
        /// Dot products of 2 vector
        /// </summary>
        /// <param name="lhs">vector 1</param>
        /// <param name="rhs">vector 2</param>
        /// <returns>Returns the cross products of the given vectors</returns>
        static double Dot(const Vector3& lhs, const Vector3& rhs);
	};
}