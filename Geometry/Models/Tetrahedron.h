#pragma once

#include "Vector3.h"

namespace Geometry
{
    //Based on: https://www.geeksforgeeks.org/program-to-find-the-volume-of-an-irregular-tetrahedron/
    // and this http://mathcentral.uregina.ca/QQ/database/QQ.09.03/peter2.html
    class  Tetrahedron
    {
    public:
         Tetrahedron(const Vector3& A, const Vector3& B, const Vector3& C, const Vector3& D);
         ~Tetrahedron() { }

         Tetrahedron(const Tetrahedron& other) { *this = other; }

         Tetrahedron& operator=(const Tetrahedron& other);

         /// <summary>
         /// Get volume
         /// </summary>
         /// <returns>Returns the volume of tetrahedron</returns>
         double GetVolume();

    private:
        //Corners
        Vector3 A, B, C, D;

        //Sides of the tetrahedron
        double a, b, c, d, e, f;
    };
}