#include "Face.h"

namespace Geometry
{
	Face::Face()
	{
	}

	Face::~Face()
	{
	}

	void Face::AddVertex(shared_ptr<Vector3>& vertex)
	{
		points.push_back(std::move(vertex));
	}

	void Face::AddTriangle(shared_ptr<Triangle>& triangle)
	{
		triangles.push_back(triangle);
	}

	Vector3 Face::GetNormal()
	{
		return Vector3::Zero;
	}
}