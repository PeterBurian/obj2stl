#include "Tetrahedron.h"
#include <cmath>

namespace Geometry
{
    Tetrahedron::Tetrahedron(const Vector3 & A, const Vector3 & B, const Vector3 & C, const Vector3 & D)
    {
        this->A = A;
        this->B = B;
        this->C = C;
        this->D = D;

        a = Vector3::Distance(B, C);
        b = Vector3::Distance(A, C);
        c = Vector3::Distance(A, B);

        d = Vector3::Distance(A, D);
        e = Vector3::Distance(B, D);
        f = Vector3::Distance(C, D);
    }

    Tetrahedron& Tetrahedron::operator=(const Tetrahedron& other)
    {
        if (this != &other)
        {
            this->A = other.A;
            this->B = other.B;
            this->C = other.C;
            this->D = other.D;

            this->a = other.a;
            this->b = other.b;
            this->c = other.c;
            this->d = other.d;
            this->e = other.e;
            this->f = other.f;
        }

        return *this;
    }

    double Tetrahedron::GetVolume()
    {
        return abs(Vector3::Dot(D - A, Vector3::Cross(B - A, C - A))) / 6.0;
    }
}