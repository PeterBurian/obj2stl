#include "Group.h"
namespace Geometry
{
	Group::Group(const string& title)
	{
		this->title = string(title);
	}

	Group& Group::operator=(const Group& other)
	{
		if (this != &other)
		{
			this->title = other.title;
		}
		return *this;
	}

	void Group::AddFace(const shared_ptr<Face>& face)
	{
		ActiveFace = face;
		faces.push_back(face);
	}
}