#include "Plane.h"

namespace Geometry
{
    Plane::Plane(const Vector3 & a, const Vector3 & b, const Vector3 & c)
    {
        Vector3 cross = Vector3::Cross(b - a, c - a);
        cross.Normalize();
        this->normal = cross;

        distance = -Vector3::Dot(normal, a);
    }

    Plane::Plane(const Vector3& normal, const Vector3& point)
    {
        Vector3 inNorm(normal);
        inNorm.Normalize();
        this->normal = inNorm;

        this->distance = -Vector3::Dot(normal, point);
    }

    Plane& Plane::operator=(const Plane& other)
    {
        if (this != &other)
        {
            this->normal = other.normal;
            this->distance = other.distance;
        }
        return *this;
    }

    Vector3 Plane::ClosestPointOnPlane(const Vector3 & point)
    {
        double pointToPlaneDistance = Vector3::Dot(normal, point) + distance;
        return point - (normal * pointToPlaneDistance);
    }

    double Plane::GetDistanceToPoint(const Vector3 & point)
    {
        return Vector3::Dot(normal, point) + distance;
    }

    bool Plane::SameSide(const Vector3 & pt1, const Vector3 & pt2)
    {
        double d0 = GetDistanceToPoint(pt1);
        double d1 = GetDistanceToPoint(pt2);
        return (d0 > 0.0f && d1 > 0.0f) || (d0 <= 0.0f && d1 <= 0.0f);
    }
}