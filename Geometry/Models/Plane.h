#pragma once

#include "Vector3.h"

namespace Geometry
{
    //Based on Unity's Plane class
    //https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Geometry/Plane.cs

    class Plane
    {
    public:

        Plane(const Vector3& a, const Vector3& b, const Vector3& c);
        Plane(const Vector3& normal, const Vector3& point);
        ~Plane() { }

        Plane(const Plane& other) { *this = other; }

        Plane& operator=(const Plane& other);

        /// <summary>
       /// Get the closest point lies on the plane
       /// </summary>
       /// <returns>Returns the closest point on the plane</returns>
        Vector3 ClosestPointOnPlane(const Vector3& point);

        /// <summary>
        /// Get the given point and the plane distance
        /// </summary>
        /// <returns>Returns a signed distance from plane to point.</returns>
        double GetDistanceToPoint(const Vector3& point);

        /// <summary>
        /// Check that the given points are on the same side of the plane
        /// </summary>
        /// <returns>Returns true if they are on the sam side</returns>
        bool SameSide(const Vector3& pt1, const Vector3& pt2);

    private:
        /// <summary>
        /// Normal of the plane
        /// </summary>
        Vector3 normal;

        double distance;
    };
}