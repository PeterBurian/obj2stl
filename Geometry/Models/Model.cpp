#include "../Interfaces/Model.h"
#include "../Triangulator/SimpleTriangulator.h"

#include <iostream>

namespace Geometry
{
	Model::Model()
	{
		ActiveGroup = nullptr;
	}

	void Model::AddGroup(const shared_ptr<Group>& group)
	{
		ActiveGroup = group;
		groups.push_back(ActiveGroup);
	}

	void Model::AddVertex(const shared_ptr<Vector3>& vector)
	{
		verticesBuffer.push_back(vector);
	}

	void Model::AddFace(const shared_ptr<Face>& face)
	{
		if (ActiveGroup == nullptr)
		{
			cout << "There is no active group!" << endl;
			AddEmptyGroup();
		}

		ActiveGroup->AddFace(face);
	}

	void Model::AddNormal(const shared_ptr<Vector3>& vector)
	{
		normals.push_back(vector);
	}

	void Model::AddTextureVertex(const shared_ptr<Vector3>& vector)
	{
		textureVertices.push_back(vector);
	}

	bool Model::GetVertexByIndex(const unsigned int idx, shared_ptr<Vector3>& vertex)
	{
		if (verticesBuffer.size() > idx)
		{
			vertex = verticesBuffer[idx];
			return true;
		}
		return false;
	}

	size_t Model::GroupsCount()
	{
		return groups.size();
	}

	void Model::CreateTriangles()
	{
		LoopFaces([&](const shared_ptr<Face>& face) {
			size_t ptsCount = face->points.size();
			if (ptsCount == 3)
			{
				//Just put them to the triangles list
				auto triangle = std::make_shared<Triangle>(face->points);
				face->AddTriangle(triangle);
			}
			else if (ptsCount > 3)
			{
				//Need to create triangles
				Triangulate(face->points, face);
			}
			else
			{
				cout << "Face contains less than 3 points! " << endl;
			}
			});
	}

	void Model::Triangulate(const vector<shared_ptr<Vector3>>& polygon, const shared_ptr<Face>& face)
	{
		vector<Vector3> poly;
		for (auto &pt : polygon)
		{
			poly.push_back(*pt);
		}
		auto triangulator = std::make_unique<SimpleTriangulator>(poly);
		
		vector<vector<int>> result;
		if (triangulator->GetTriangles(result))
		{
			for (auto &tri : result)
			{
				vector<shared_ptr<Vector3>> corners;

				corners.push_back(polygon[tri[0]]);
				corners.push_back(polygon[tri[1]]);
				corners.push_back(polygon[tri[2]]);
				auto triangle = std::make_shared<Triangle>(corners);
				face->AddTriangle(triangle);
			}
		}
	}

	void Model::ClearStoredVertices()
	{
		verticesBuffer.clear();
	}

	size_t Model::GetNumberOfTriangles()
	{
		size_t numberOfTriangles = 0;

		LoopFaces([&](const shared_ptr<Face>& face) {
			numberOfTriangles += face->triangles.size();
			});

		return numberOfTriangles;
	}

    Vector3 Model::GetCenter()
    {
		Geometry::Vector3 min, max;
		GetExtents(min, max);

		return (min + max) / 2;
    }

    void Model::GetExtents(Vector3 & min, Vector3 & max)
    {
        if (verticesBuffer.size() > 0)
        {
            min = *verticesBuffer[0];
            max = *verticesBuffer[0];

            for (size_t i = 1; i < verticesBuffer.size(); i++)
            {
                Vector3 pt = *verticesBuffer[i];
                if (pt.x < min.x) min.x = pt.x;
                if (pt.y < min.y) min.y = pt.y;
                if (pt.z < min.z) min.z = pt.z;

                if (pt.x > max.x) max.x = pt.x;
                if (pt.y > max.y) max.y = pt.y;
                if (pt.z > max.z) max.z = pt.z;
            }
        }
        else
        {
            std::cout << "The vertexbuffer is empty" << std::endl;
        }
    }

	void Model::AddEmptyGroup()
	{
		const auto group = std::make_shared<Geometry::Group>();
		AddGroup(group);
	}
}