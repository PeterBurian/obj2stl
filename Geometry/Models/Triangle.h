#pragma once

#include "Vector3.h"
#include "../Interfaces/BaseModel.h"
#include <memory>
#include <vector>

using namespace std;
namespace Geometry
{
	class Triangle : public BaseModel
	{
	public:
		Triangle() { }
		Triangle(vector<shared_ptr<Vector3>> items);
		~Triangle() { }

		Triangle(const Triangle& other) { *this = other; }

		Triangle& operator=(const Triangle& other);

		//Corner A
		const Vector3 A();

		//Corner B
		const Vector3 B();

		//Corner C
		const Vector3 C();

		/// <summary>
		/// Check the triangle
		/// </summary>
		/// <returns>Returns true if the triangle has all the 3 points</returns>
		bool IsValid();

		/// <summary>
		/// Get the area
		/// </summary>
		/// <returns>Returns the area of the triangle</returns>
		double Area();

		/// <summary>
		/// Get normal vector of the triangle
		/// </summary>
		/// <returns>Returns the normal vector</returns>
		Vector3 Normal();

        /// <summary>
        /// Get the arithmetic mean of the triangle
        /// </summary>
        /// <returns>Returns the center of the triangle</returns>
        Vector3 Center();

        /// <summary>
        /// Get the area of a triangle from the 3 corners
        /// </summary>
        /// <returns>Returns the area of the triangle</returns>
        static double Area(const Vector3& A, const Vector3& B, const Vector3& C);

        /// <summary>
        /// Get the area of a triangle from the 3 sides
        /// </summary>
        /// <returns>Returns the area of the triangle</returns>
        static double Area(const double a, const double b, const double c);

	private:
		vector<shared_ptr<Vector3>> corners;

		Vector3 GetCoordByIdx(const unsigned int idx);
	};
}