#include "SimpleTriangulator.h"

namespace Geometry
{
	SimpleTriangulator::SimpleTriangulator(const vector<Vector3>& polygon)
	{
		//Copy polygon
		poly = vector<Vector3>(polygon);
	}

	bool SimpleTriangulator::GetTriangles(vector<vector<int>>& triIndicies)
	{
		if (poly.size() <= 3)
		{
			return false;
		}
		else
		{
			triIndicies.clear();

			int baseIdx = 0;

			for (int i = 2; i < poly.size(); i++)
			{
				const vector<int> triIdxs{ baseIdx, i - 1, i };
				triIndicies.push_back(triIdxs);
			}
			return true;
		}
	}
}