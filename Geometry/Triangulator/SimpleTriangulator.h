#pragma once

#include "../Models/Vector3.h"

#include <vector>

using namespace std;
namespace Geometry
{
	/// <summary>
	/// This is a very simple triangulator based on earcut algo.
	/// I don't want to use an external libs or classes a I wrote before (Delaunay, Ear clipping), beacuse they were complex.
	/// And I think this the property of my former company ;)
	/// So I created a new basic one.
	/// It will works with concave polygons without holes and probably it creates narrow triangles.
	/// </summary>
	class SimpleTriangulator
	{
	public:
		SimpleTriangulator(const vector<Vector3>& polygon);
		
		bool GetTriangles(vector<vector<int>>& triIndicies);

	private:
		vector<Vector3> poly;
	};
}