#include "../Interfaces/STLConverter.h"

#include <filesystem>
#include <iostream>

namespace Converter
{
	STLConverter::STLConverter(const string& filePath)
	{
		SetExtension(filePath);
	}

	STLConverter& STLConverter::operator=(const STLConverter& other)
	{
		if (this != &other)
		{
			this->stlPath = other.stlPath;
		}
		return *this;
	}

	bool STLConverter::Convert(const std::shared_ptr<Geometry::Model>& model)
	{
		std::ofstream oFstr(stlPath, std::ostream::binary);
		if (oFstr.is_open())
		{
			size_t numberOfTriangles = model->GetNumberOfTriangles();
			unsigned int count = static_cast<unsigned int>(numberOfTriangles);

			AddHeader(oFstr, count);
			AddBody(oFstr, model);
			oFstr.close();
			return true;
		}
		else
		{
			cout << "File can't be opened!" << endl;
			return false;
		}
	}

	void STLConverter::SetExtension(const string& filePath)
	{
		std::filesystem::path path = filePath;
		path.replace_extension(".stl");
		stlPath = path.string();
	}

	void STLConverter::AddHeader(ostream& os, unsigned int triCount) const
	{
		/*
		UINT8[80]    � Header                 -     80 bytes                           
		UINT32       � Number of triangles    -      4 bytes
		*/

		const char header[80] = { 0 };
		os.write(header, sizeof(header));
		os.write(reinterpret_cast<const char*>(&triCount), sizeof(uint32_t));
	}
	
	void STLConverter::AddBody(ostream& os, const shared_ptr<Geometry::Model>& model) const
	{
		/*
		foreach triangle                      - 50 bytes:
			REAL32[3] � Normal vector             - 12 bytes
			REAL32[3] � Vertex 1                  - 12 bytes (double(8byte) -> float(4byte))
			REAL32[3] � Vertex 2                  - 12 bytes
			REAL32[3] � Vertex 3                  - 12 bytes
			UINT16    � Attribute byte count      -  2 bytes
		end
		*/

		model->LoopTriangles([&](const shared_ptr<Geometry::Triangle>& triangle) 
			{
				PutCoords(os, triangle->Normal());
				PutCoords(os, triangle->A());
				PutCoords(os, triangle->B());
				PutCoords(os, triangle->C());

				PutAttribute(os);
			});
	}

	void STLConverter::PutCoords(ostream& os, const Geometry::Vector3& vector) const
	{
		float x = static_cast<float>(vector.x);
		os.write(reinterpret_cast<const char*>(&x), sizeof(float));

		float y = static_cast<float>(vector.y);
		os.write(reinterpret_cast<const char*>(&y), sizeof(float));

		float z = static_cast<float>(vector.z);
		os.write(reinterpret_cast<const char*>(&z), sizeof(float));
	}

	inline void STLConverter::PutAttribute(ostream& os) const
	{
		const char attribute[2] = { 0 };
		os.write(reinterpret_cast<const char*>(attribute), sizeof(attribute));
	}
}