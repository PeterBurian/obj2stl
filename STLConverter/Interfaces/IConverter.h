#pragma once

#include <memory>
#include "Model.h"

namespace Converter
{
	class IConverter
	{
	public:
		virtual ~IConverter() { }

		virtual bool Convert(const std::shared_ptr<Geometry::Model>& model) = 0;
	};
}