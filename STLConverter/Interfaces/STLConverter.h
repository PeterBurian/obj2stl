#pragma once

#include "IConverter.h"
#include "Model.h"

#include <fstream>
#include <string>

using namespace std;
namespace Converter
{
	class STLConverter : public IConverter
	{
	public:
		STLConverter(const string& filePath);
		~STLConverter() { }

		STLConverter(const STLConverter& other) { *this = other; }
		
		STLConverter& operator=(const STLConverter& other);

		/// <summary>
		/// Convert model to stl.
		/// The output file will place next to the original obj file 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		bool Convert(const shared_ptr<Geometry::Model>& model) override;
	
	private:
		string stlPath;

		/// <summary>
		/// Set extension to stl
		/// </summary>
		/// <param name="filePath"></param>
		void SetExtension(const string& filePath);

		/// <summary>
		/// Create STL header
		/// </summary>
		/// <param name="os">output filestream</param>
		/// <param name="triCount">number of triangles</param>
		void AddHeader(ostream& os, unsigned int triCount) const;

		/// <summary>
		/// Add STL body
		/// </summary>
		/// <param name="os">output filestream</param>
		/// <param name="model">model</param>
		void AddBody(ostream& os, const shared_ptr<Geometry::Model>& model) const;

		/// <summary>
		/// Write coordinates to stl
		/// It converts the coordinates to float
		/// </summary>
		/// <param name="os">output filestream</param>
		/// <param name="vector">vector</param>
		void PutCoords(ostream& os, const Geometry::Vector3& vector) const;
		
		/// <summary>
		/// Add a dummy attribute to the end of the block
		/// </summary>
		/// <param name="os"></param>
		inline void PutAttribute(ostream& os) const;
	};
}