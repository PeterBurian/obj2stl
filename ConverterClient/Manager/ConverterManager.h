#pragma once

#include "IParser.h"
#include "IConverter.h"
#include "ITransfromation.h"

#include <list>
#include <memory>

using namespace std;
namespace Client
{
	class ConverterManager
	{
		public:
			ConverterManager(shared_ptr<Parser::IParser>& parser, list<shared_ptr<Geometry::ITransfromation>>* transforms, shared_ptr<Converter::IConverter>& converter);
			~ConverterManager();

			bool Execute();

		private:
			shared_ptr<Parser::IParser> mParser;
			shared_ptr<Converter::IConverter> mConverter;
			const list<shared_ptr<Geometry::ITransfromation>>* mTransforms;

			void PrintResults(const shared_ptr<Geometry::Model>& model);
	};
}