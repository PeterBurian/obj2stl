#include "ConverterManager.h"
#include "Calculator.h"
#include <iostream>

namespace Client
{
	ConverterManager::ConverterManager(std::shared_ptr<Parser::IParser>& parser, list<shared_ptr<Geometry::ITransfromation>>* transforms, std::shared_ptr<Converter::IConverter>& converter)
	{
		mParser = parser;
		mConverter = converter;
		mTransforms = transforms;
	}

	ConverterManager::~ConverterManager()
	{
		mParser = nullptr;
		mConverter = nullptr;
		mTransforms = nullptr;
	}

	bool ConverterManager::Execute()
	{
		if (mParser != nullptr && mConverter != nullptr)
		{
			std::cout << "Execute !!!" << std::endl;

			auto model = std::make_shared<Geometry::Model>();

			if (mParser->Parse(model))
			{
				// Do transforms in given order
				for (auto it = mTransforms->begin(); it != mTransforms->end(); ++it) 
				{
					// Maybe need to check the transform was success or not
					// What should happens if the transform fail?
					(*it)->DoTransform(model);
				}

				PrintResults(model);

				//Convert the result
				return mConverter->Convert(model);
			}
			return false;
		}
		else
		{
			std::cout << "Parser and/or converter can not be null" << std::endl;
			return false;
		}
	}

	void ConverterManager::PrintResults(const shared_ptr<Geometry::Model>& model)
	{
		auto calculator = std::make_unique<Geometry::Calculator>();

#pragma region Surface

		/// Print the surface of the model
		double surface = calculator->Surface(model);
		std::cout << "The surface of the model: " << surface << std::endl;


#pragma endregion


#pragma region Volume

		double volume = calculator->Volume(model);
		std::cout << "The volume of the model: " << volume << std::endl;

#pragma endregion


#pragma region Is point in model

		//Test data 1
		Geometry::Vector3 center = model->GetCenter(); //--> this will be inside

		Geometry::Vector3 min, max;
		model->GetExtents(min, max);

		//Test data 2
		max = max + Geometry::Vector3::One; //--> this will be outside

		//Test data 3
		min = min - Geometry::Vector3::One; //--> this will be outside

		bool isIn = calculator->IsPointInModel(center, model);
		std::cout << "The point is : " << (isIn ? "inside" : "outside") << std::endl;

#pragma endregion
	}
}