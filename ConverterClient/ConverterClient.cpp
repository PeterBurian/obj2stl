#include "IParser.h"
#include "ObjWFParser.h"
#include "IConverter.h"
#include "STLConverter.h"
#include "Manager/ConverterManager.h"
#include "Rotate.h"
#include "Scale.h"
#include "Translate.h"

#include <iostream>
#include <string>
#include <list>
#include <memory>

static void Execute(std::string& filePath)
{
    auto parser = std::shared_ptr<Parser::IParser>(new Parser::ObjWFParser(filePath));
    auto converter = std::shared_ptr<Converter::IConverter>(new Converter::STLConverter(filePath));

    auto rotate = std::shared_ptr<Geometry::Rotate>(new Geometry::Rotate(Geometry::Vector3(3.1415, 0, 0)));
    auto scale = std::shared_ptr<Geometry::Scale>(new Geometry::Scale(Geometry::Vector3(8., 1, 1)));
    auto translate = std::shared_ptr<Geometry::Translate>(new Geometry::Translate(Geometry::Vector3(10, 5, 0)));

    std::list<shared_ptr<Geometry::ITransfromation>>* transforms = new std::list<shared_ptr<Geometry::ITransfromation>> { /* scale, rotate */ };

    auto manager = std::unique_ptr<Client::ConverterManager>(new Client::ConverterManager(parser, transforms, converter));
    manager->Execute();

    delete transforms;
}

/// <summary>
/// Entry point of the application
/// </summary>
/// <returns></returns>
int main(int argc, char* argv[])
{
    if (argc == 2)
    {
        std::string objPath(argv[1]);
        std::cout << "File path: " << objPath.c_str() << std::endl;

        Execute(objPath);
    }
    else
    {
        std::cout << "Parameter is not valid!." << std::endl;
    }
    
    return 0;
}