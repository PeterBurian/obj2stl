#pragma once

#include "ObjLineParser.h"

namespace Parser
{
    class ObjCommentParser : public ObjLineParser
    {
    public:
        ObjCommentParser() { }

        ~ObjCommentParser() { }

        bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) override;
    };
}