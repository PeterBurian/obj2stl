#include "ObjVertexNormalParser.h"

namespace Parser
{
	bool ObjVertexNormalParser::ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model)
	{
		size_t dataLength = objLineData.size();
		if (dataLength <= 4)
		{
			double i = stod(objLineData[1]);
			double j = stod(objLineData[2]);
			double k = stod(objLineData[3]);

			auto vertex = std::make_shared<Geometry::Vector3>(i, j, k);
			model->AddNormal(std::move(vertex));

			return true;
		}
		return false;
	}
}