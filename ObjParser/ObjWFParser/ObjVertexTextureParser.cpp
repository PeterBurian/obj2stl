#include "ObjVertexTextureParser.h"

namespace Parser
{
	bool ObjVertexTextureParser::ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model)
	{
		size_t dataLength = objLineData.size();
		if (dataLength <= 4)
		{
			double u = stod(objLineData[1]);
			double v = stod(objLineData[2]);
			double w = stod(objLineData[3]);

			auto vertex = std::make_shared<Geometry::Vector3>(u, v, w);
			model->AddTextureVertex(std::move(vertex));

			return true;
		}
		return false;
	}
}