#pragma once

#include "Model.h"

#include <string>
#include <vector>
#include <memory>

using namespace std;
namespace Parser
{
    class ObjLineParser
    {
        public:
            virtual ~ObjLineParser() { }

            /// <summary>
            /// Parsing the given line with the right parser
            /// </summary>
            /// <param name="objLineData">line</param>
            /// <param name="model">model</param>
            /// <returns>Returns true if the processing was success</returns>
            virtual bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) = 0;
    };
}