#pragma once

#include "ObjLineParser.h"

namespace Parser
{
    class ObjVertexParser : public ObjLineParser
    {
    public:
        ObjVertexParser() { }

        ~ObjVertexParser() { }

        bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) override;
    };
}