#pragma once

#include <string>

using namespace std;
namespace Parser
{
    class Tag
    {
    public:
        static const string UNDEFINED;
        static const string COMMENT;
        static const string GROUP;
        static const string FACE;
        static const string VERTEX;
        static const string VERTEX_NORMAL;
        static const string VERTEX_TEXTURE;
    };
}