#pragma once

#include "ObjLineParser.h"

namespace Parser
{
    class ObjFaceParser : public ObjLineParser
    {
    public:
        ObjFaceParser() {}

        ~ObjFaceParser() {}

        bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) override;
    private:

    };
}