#pragma once

#include "ObjLineParser.h"

namespace Parser
{
	class ObjVertexTextureParser : public ObjLineParser
	{
	public:
		ObjVertexTextureParser() { }

		~ObjVertexTextureParser() { }

		bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) override;
	};
}