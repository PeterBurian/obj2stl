#include "ObjFaceParser.h"
#include "Vector3.h"
#include "Face.h"

#include <iostream>

namespace Parser
{
	bool ObjFaceParser::ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model)
	{
		size_t fSize = objLineData.size();

		// (f, idx1, idx2, idx3, ...)
		// We need at least 3 point in a face
		if (objLineData.size() >= 4)
		{
			const auto face = std::make_shared<Geometry::Face>();
			for (size_t i = 1; i < fSize; i++)
			{
				unsigned int idx = stoi(objLineData[i]) - 1; // The index of the obj file starts from 1

				shared_ptr<Geometry::Vector3> vertex = nullptr;

				if (model->GetVertexByIndex(idx, vertex))
				{
					face->AddVertex(vertex);
				}
				else
				{
					std::cout << "Vertex is not accesibble!" << endl;
				}
			}
			model->AddFace(face);
			return true;
		}
		return false;
	}
}