#include "ObjVertexParser.h"
#include "Vector3.h"

namespace Parser
{
	bool ObjVertexParser::ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model)
	{
		double x;
		double y;
		double z;
		double w = 1.0;

		size_t dataLength = objLineData.size();
		if (dataLength <= 5)
		{
			// v, X, Y, Z , (W)
			x = stod(objLineData[1]);
			y = stod(objLineData[2]);
			z = stod(objLineData[3]);
			
			//Now I skip w...
			auto vertex = std::make_shared<Geometry::Vector3>(x, y, z);
			model->AddVertex(std::move(vertex));

			return true;
		}
		return false;
	}
}