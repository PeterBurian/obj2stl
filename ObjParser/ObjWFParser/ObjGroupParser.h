#pragma once

#include "ObjLineParser.h"

namespace Parser
{
    class ObjGroupParser : public ObjLineParser
    {
    public:
        ObjGroupParser() {}

        ~ObjGroupParser() {}

        bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) override;
    };
}