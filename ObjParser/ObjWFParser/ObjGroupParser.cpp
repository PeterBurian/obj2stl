#include "ObjGroupParser.h"
#include "Group.h"

namespace Parser
{
	bool ObjGroupParser::ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model)
	{
		string groupName;
		if (objLineData.size() == 2)
		{
			groupName = objLineData[1];
		}

		const auto group = std::make_shared<Geometry::Group>(groupName);
		model->AddGroup(group);

		return true;
	}
}