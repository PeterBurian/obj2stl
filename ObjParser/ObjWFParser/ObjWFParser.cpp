#include "../Interfaces/ObjWFParser.h"
#include "ObjLineParser.h"

#include <iostream>
#include <fstream>

namespace Parser
{
	ObjWFParser::ObjWFParser(string& filePath)
	{
		mFilePath = filePath;
	}

	ObjWFParser& ObjWFParser::operator=(const ObjWFParser& other)
	{
		if (this != &other)
		{
			this->mFilePath = other.mFilePath;
		}
		return *this;
	}

	bool ObjWFParser::Parse(shared_ptr<Geometry::Model>& model)
	{
		if (!mFilePath.empty())
		{
			auto tagManager = make_shared<TagManager>();

			ifstream file(mFilePath.c_str(), ifstream::in);
			if (file.is_open())
			{
				string line;
				while (getline(file, line))
				{
					ParseLine(line, tagManager, model);
				}
				
				model->CreateTriangles();
				file.close();

				return true;
			}
			else
			{
				cout << "File can not be opened!" << endl;
			}
		}
		else
		{
			cout << "File path is not set!" << endl;
		}
		return false;
	}

	bool ObjWFParser::ParseLine(string& line, shared_ptr<TagManager>& tagManager, shared_ptr<Geometry::Model>& model)
	{
		vector<string> parts;

		if (Split(line, parts))
		{
			shared_ptr<ObjLineParser> parser = nullptr;

			if (tagManager->GetParser(parts[0], parser))
			{
				return parser->ProcessLine(parts, model);
			}
		}
		return false;
	}

	bool ObjWFParser::Split(string& line, vector<string>& result)
	{
		const string& delimiter = " ";
		size_t pos = 0;
		string token;
		while ((pos = line.find(delimiter)) != string::npos)
		{
			token = line.substr(0, pos);
			if (IsPartValid(token))
			{
				result.push_back(token);
			}
			line.erase(0, pos + delimiter.length());
		}

		if (IsPartValid(line))
		{
			result.push_back(line);
		}

		return result.size() > 0;
	}

	bool inline ObjWFParser::IsPartValid(const string& line)
	{
		return (!line.empty() && line != "");
	}
}