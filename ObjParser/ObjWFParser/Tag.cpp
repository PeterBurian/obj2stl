#include "Tag.h"

namespace Parser
{
	const string Tag::UNDEFINED = "";
	const string Tag::COMMENT = "#";
	const string Tag::GROUP = "g";
	const string Tag::FACE = "f";
	const string Tag::VERTEX = "v";
	const string Tag::VERTEX_NORMAL = "vn";
	const string Tag::VERTEX_TEXTURE = "vt";
}