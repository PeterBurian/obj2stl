#pragma once

#include "ObjLineParser.h"

namespace Parser
{
	class ObjVertexNormalParser : public ObjLineParser
	{
	public:
		ObjVertexNormalParser() { }

		~ObjVertexNormalParser() { }

		bool ProcessLine(const vector<string>& objLineData, const shared_ptr<Geometry::Model>& model) override;
	};
}