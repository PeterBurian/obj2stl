#pragma once

#include "../ObjWFParser/ObjLineParser.h"
#include "../ObjWFParser/Tag.h"

#include <vector>
#include <map>
#include <memory>

namespace Parser
{
    class TagManager
    {
    public:
        TagManager();
        ~TagManager() { parsers.clear(); }

        /// <summary>
        /// Get the parser from the given tag
        /// </summary>
        /// <param name="tag"></param>
        /// <param name="parser"></param>
        /// <returns>Returns true if the parser is found otherwise false</returns>
        bool GetParser(const string& tag, shared_ptr<ObjLineParser>& parser);

    private:
        /// <summary>
        /// These are the avaible parser tags
        /// </summary>
        const vector<string> avaibleTags { Tag::GROUP, Tag::FACE, Tag::VERTEX, Tag::VERTEX_NORMAL, Tag::VERTEX_TEXTURE };
        
        /// <summary>
        /// Collection of parsers
        /// </summary>
        map<const string, shared_ptr<ObjLineParser>> parsers;
        
        /// <summary>
        /// Generate collection of parsers
        /// </summary>
        void GenerateTagMapping();
    };
}