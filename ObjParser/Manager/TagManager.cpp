#include "TagManager.h"
#include "../ObjWFParser/ObjVertexParser.h"
#include "../ObjWFParser/ObjGroupParser.h"
#include "../ObjWFParser/ObjFaceParser.h"
#include "../ObjWFParser/ObjCommentParser.h"
#include "../ObjWFParser/ObjVertexNormalParser.h"
#include "../ObjWFParser/ObjVertexTextureParser.h"

#include <string>

namespace Parser
{
    TagManager::TagManager()
    {
        GenerateTagMapping();
    }

    bool TagManager::GetParser(const string& tag, shared_ptr<ObjLineParser>& parser)
    {
        auto it = parsers.find(tag);
        if (it != parsers.end())
        {
            parser = (*it).second;
            return true;
        }
        return false;
    }

    void TagManager::GenerateTagMapping()
    {
        parsers.clear();

        for (const auto &tag : avaibleTags)
        {
            if (tag == Tag::COMMENT) { parsers.insert(make_pair(Tag::COMMENT, shared_ptr<ObjCommentParser>(new ObjCommentParser()))); }
            if (tag == Tag::GROUP) { parsers.insert(make_pair(Tag::GROUP, shared_ptr<ObjGroupParser>(new ObjGroupParser()))); }
            if (tag == Tag::FACE) { parsers.insert(make_pair(Tag::FACE, shared_ptr<ObjFaceParser>(new ObjFaceParser()))); }
            if (tag == Tag::VERTEX) { parsers.insert(make_pair(Tag::VERTEX, shared_ptr<ObjVertexParser>(new ObjVertexParser()))); }
            if (tag == Tag::VERTEX_NORMAL) { parsers.insert(make_pair(Tag::VERTEX_NORMAL, shared_ptr<ObjVertexNormalParser>(new ObjVertexNormalParser()))); }
            if (tag == Tag::VERTEX_TEXTURE) { parsers.insert(make_pair(Tag::VERTEX_TEXTURE, shared_ptr<ObjVertexTextureParser>(new ObjVertexTextureParser()))); }
        }
    }
}