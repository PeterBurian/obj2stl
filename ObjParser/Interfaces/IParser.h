#pragma once

#include "Model.h"
#include <memory>

namespace Parser
{
	class IParser
	{
	public:
		virtual ~IParser() { }

		virtual bool Parse(std::shared_ptr<Geometry::Model>& model)=0;
	};
}