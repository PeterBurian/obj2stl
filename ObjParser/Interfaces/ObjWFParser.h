#pragma once

#include "IParser.h"
#include "../Manager/TagManager.h"
#include "Model.h"

#include <string>
#include <vector>
#include <memory>

using namespace std;

namespace Parser
{
	/// <summary>
	/// Wavefront OBJ Parser
	/// </summary>
	class ObjWFParser : public IParser
	{
		public:
			ObjWFParser(string& filePath);
			~ObjWFParser() { }

			ObjWFParser(const ObjWFParser& other) { *this = other; }

			ObjWFParser& operator=(const ObjWFParser& other);

			/// <summary>
			/// Parse the given Wavefront obj to the given model
			/// </summary>
			/// <param name="model">model</param>
			/// <returns>Returns true if the parse and the conversion is success</returns>
			bool Parse(shared_ptr<Geometry::Model>& model) override;

		private:
			string mFilePath;

			/// <summary>
			/// Parse the given line
			/// </summary>
			/// <param name="line">line</param>
			/// <param name="tagManager">tagManager</param>
			/// <param name="model">model</param>
			/// <returns>Returns true if the line parsing is success</returns>
			bool ParseLine(string& line, shared_ptr<TagManager>& tagManager, shared_ptr<Geometry::Model>& model);

			/// <summary>
			/// Split line by character space
			/// </summary>
			/// <param name="line">line</param>
			/// <param name="result">result</param>
			/// <returns>Returns true if the parse is success</returns>
			bool Split(string& line, vector<string>& result);

			/// <summary>
			/// Check the validity of the given line
			/// </summary>
			/// <param name="line">line</param>
			/// <returns>Returns true if the line is not empty otherwise false</returns>
			bool IsPartValid(const string& line);
	};
}
